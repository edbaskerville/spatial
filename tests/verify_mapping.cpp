// -*- C++ -*-
//
// Copyright Sylvain Bougerel 2009 - 2012.
// Distributed under the Boost Software License, Version 1.0.
// (See accompanying file COPYING or copy at
// http://www.boost.org/LICENSE_1_0.txt)

/**
 *  @file   verify_mapping.cpp
 *  @brief  Test all functions associated to the 'mapping' family of iterators.
 */

#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

#include "bits/spatial_test_fixtures.hpp"
#include "bits/spatial_test_mapping.hpp"
